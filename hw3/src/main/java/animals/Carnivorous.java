package animals;
// хищник

import aviary.SizeAviary;
import food.Food;
import food.Grass;
import food.WrongFoodException;

public class Carnivorous extends Animal {

    public Carnivorous(String name, SizeAviary size) {
        super(name, size);
    }

    @Override
    public void eat(Food food) throws WrongFoodException {

        if (food instanceof Grass) {
            throw new WrongFoodException(this.getName() + " не ест" + food.nameFood());
        } else {
            System.out.println("Удача, " + food.nameFood() + " подходит животному ");
            System.out.println("Он благодарно смотрит на вас, потому что теперь сыт.");
            addSatiety(food.addSatiety());
            System.out.println("Его сытость составляет: " + getSatiety());
        }
    }

}





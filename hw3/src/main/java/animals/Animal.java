package animals;

import aviary.SizeAviary;
import food.Food;
import food.WrongFoodException;

import java.util.Objects;

public abstract class Animal {

    private String name;
    private SizeAviary aviarySize;

    public SizeAviary getSize() {
        return aviarySize;
    }

    public abstract void eat(Food food) throws WrongFoodException;

    public Animal(String name, SizeAviary aviarySize) {
        this.name = name;
        this.aviarySize = aviarySize;
    }


    private int satiety = 0;

    public int getSatiety() {
        return satiety;
    }

    public void addSatiety(int addSatietyFood) {
        satiety += addSatietyFood;
    }


    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return name.equals(animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

}



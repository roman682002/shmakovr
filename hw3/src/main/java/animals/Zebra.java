package animals;

import aviary.SizeAviary;

// зебра
public class Zebra extends Herbivorous implements Voice, Swim, Run {

    public Zebra(String name, SizeAviary size) {
        super(name, size);
    }

    @Override
    public String voice() {
        return "Зебра говорит УАУАУАУА";
    }

    @Override
    public boolean swim() {
        System.out.println("Зебра поплыла");
        return true;
    }

    @Override
    public boolean run() {
        System.out.println("Зебра побежала");
        return true;
    }

}

package animals;

import aviary.SizeAviary;

// рыба
public class Fish extends Carnivorous implements Swim {

    public Fish(String name, SizeAviary size) {
        super(name, size);
    }

    @Override
    public boolean swim() {
        System.out.println("Рыба поплыла");
        return true;
    }
}

package animals;

import aviary.SizeAviary;

// верблюд
public class Camel extends Herbivorous implements Voice, Swim, Run {


    public Camel(String name, SizeAviary size) {
        super(name, size);
    }


    @Override
    public String voice() {
        return "Верблюд говорит УАААА";
    }

    @Override
    public boolean swim() {
        System.out.println("Верблюд поплыл");
        return true;
    }

    @Override
    public boolean run() {
        System.out.println("Верблюд побежал");
        return true;
    }
}

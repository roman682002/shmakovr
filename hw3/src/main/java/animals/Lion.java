package animals;

import aviary.SizeAviary;

// Лев
public class Lion extends Carnivorous implements Voice, Swim, Run {


    public Lion(String name, SizeAviary size) {
        super(name, size);
    }

    @Override
    public String voice() {
        return "Лев говорит РАААААААР";
    }

    @Override
    public boolean swim() {
        System.out.println("Лев поплыл");
        return true;
    }

    @Override
    public boolean run() {
        System.out.println("Лев побежал");
        return true;
    }

}



import animals.*;
import aviary.Aviary;
import aviary.SizeAviary;
import food.Food;
import food.Grass;
import food.Meat;
import food.WrongFoodException;
import worker.Worker;

public class Zoo {

    public static void main(String[] args) {

        // Создание животных
        Camel camel = new Camel("Nikola", SizeAviary.HUGE);
        Duck duck = new Duck("Alya", SizeAviary.MEDIUM);
        Zebra zebra = new Zebra("Gosha", SizeAviary.LARGE);
        Fish fish = new Fish("Dasha", SizeAviary.SMALL);
        Lion lion = new Lion("Oleg", SizeAviary.LARGE);
        Harpy harpy = new Harpy("Timosha", SizeAviary.LARGE);

        // Создание еды для животных
        Food chicken = new Meat();
        Food beef = new Meat();
        Food cabbage = new Grass();
        Food potatoes = new Grass();

        // Создание рабочего
        Worker Anton = new Worker();

        //Рабочий кормит животных
        try {
            //Anton.feed(potatoes, lion); - вызывается исключение.
            Anton.feed(chicken, lion);
            // Anton.feed(chicken, duck); - вызывается исключение.
            Anton.feed(beef, lion);
            Anton.feed(beef, lion);
            Anton.feed(beef, lion);
            //Anton.feed(potatoes, fish); - вызывается исключение.
            Anton.feed(cabbage, camel);
        } catch (WrongFoodException e) {
            System.out.println(e.getMessage());
        }

        // Рабочий просит поговорить животных
        Anton.getVoice(lion);
        //Anton.getVoice(fish); - программа не компилируется
        Anton.getVoice(zebra);
        Anton.getVoice(duck);
        Anton.getVoice(harpy);

        // Пруд с животными, которые умеют плавать (массив)
        Swim[] swims = new Swim[5];
        swims[0] = lion;
        swims[1] = camel;
        swims[2] = zebra;
        swims[3] = fish;
        swims[4] = duck;

        // каждое животное плавает по очереди
        for (int i = 0; i < swims.length; i++) {
            swims[i].swim();
        }

        Aviary<Carnivorous> aviaryCarnivorous = new Aviary<>(SizeAviary.HUGE);
        Aviary<Herbivorous> aviaryHerbivorous = new Aviary<>(SizeAviary.LARGE);
        //aviaryCarnivorous.addAnimal(zebra, zebra.getSize()); - не компилируется, к хищникам нельзя добавить травоядных
        //aviaryHerbivorous.addAnimal(lion, lion.getSize()); - не компилируется, к травоядным нельзя добавить хищников

        aviaryHerbivorous.addAnimal(camel); //- животное велико
        aviaryHerbivorous.addAnimal(duck);
        System.out.println("Животное с ссылкой " + aviaryHerbivorous.getAnimal(duck.getName()) + " добавленно в варьер");
        aviaryHerbivorous.addAnimal(zebra);
        System.out.println("Животное с ссылкой " + aviaryHerbivorous.getAnimal(zebra.getName()) + " добавленно в варьер");
        aviaryCarnivorous.addAnimal(fish);
        System.out.println("Животное с ссылкой " + aviaryHerbivorous.getAnimal(fish.getName()) + " добавленно в варьер");
        aviaryCarnivorous.addAnimal(lion);
        System.out.println("Животное с ссылкой " + aviaryCarnivorous.getAnimal(lion.getName()) + " добавленно в варьер");
        aviaryCarnivorous.addAnimal(harpy);
        System.out.println("Животное с ссылкой " + aviaryCarnivorous.getAnimal(harpy.getName()) + " добавленно в варьер");

    }
}






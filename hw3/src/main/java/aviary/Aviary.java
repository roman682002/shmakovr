package aviary;

import animals.Animal;

import java.util.HashSet;
import java.util.Set;

// обобщенный класс
public class Aviary<T extends Animal> {

    private Set<T> aviaryForAnimal = new HashSet<>();
    private SizeAviary sizeAviary;

    public Aviary(SizeAviary aviarySize) {
        this.sizeAviary = aviarySize;
    }

    // добавить животное в вольер
    public void addAnimal(T animal) {
        if (animal.getSize().getSize() <= sizeAviary.getSize()) {
            System.out.println("Размер животного: " + animal.getSize());
            System.out.println("Размер вальера: " + sizeAviary.getSize());
            aviaryForAnimal.add(animal);
        } else {
            System.out.println("Животное слишком маленькое для этого барьера. Размер животного: " + animal.getSize());
            System.out.println("Минимальный размер животного должен быть равен: " + sizeAviary.getSize());
        }
    }

    // удалить животной из вольера
    public void removeAnimal(T animal) {
        aviaryForAnimal.remove(animal);
    }

    // получить ссылку на животное по его кличке
    public T getAnimal(String name) {
        for (T animal : aviaryForAnimal) {
            if (animal.getName().equals(name)) {
                return animal;
            }
        }
        return null;
    }

}
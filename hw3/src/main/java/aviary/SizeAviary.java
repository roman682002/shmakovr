package aviary;

public enum SizeAviary {
    SMALL(1),
    MEDIUM(2),
    LARGE(3),
    HUGE(4);

    private double size;

    SizeAviary(double size) {
        this.size = size;
    }

    public double getSize() {
        return size;
    }
}


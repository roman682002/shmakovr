package food;

public class Meat extends Food {

    @Override
    public String nameFood() {
        return "мясо";
    }

    @Override
    public int addSatiety() {
        return 10;
    }
}

package food;

public class Grass extends Food {


    @Override
    public String nameFood() {
        return "овощи";
    }

    @Override
    public int addSatiety() {
        return 5;
    }

}

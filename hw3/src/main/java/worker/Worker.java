package worker;

import animals.Animal;
import animals.Voice;
import food.Food;
import food.WrongFoodException;


public class Worker {

    public void feed(Food food, Animal animal) throws WrongFoodException {
        animal.eat(food);
    }

    public void getVoice(Voice animal) {
        System.out.println(animal.voice());
    }

}


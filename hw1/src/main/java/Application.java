import model.Kotik;

public class Application {
    public static void main(String[] args) {
        // создание 2 объектов класса котика
        Kotik Cat1 = new Kotik(10, 10, "Marsel", "mmwau", 1);
        Kotik Cat2 = new Kotik();
        Cat2.setKotik(10, 10, "Sanya", "muuau", 0);

        // Примеры методов
        Cat1.liveAnotherDay();

        System.out.println(Cat2.getName() + "  has a weight of " + Cat2.getWeight());

        if (Cat1.getMeow().equals(Cat2.getMeow())) {
            System.out.println("Cats meow the same way");
        } else {
            System.out.println("Cats have different meows");
            System.out.println(Cat1.getName() + " meows: " + Cat1.getMeow());
            System.out.println(Cat2.getName() + " meows: " + Cat2.getMeow());
        }

        System.out.println("Created cats: " + Cat2.getCount());

    }
}


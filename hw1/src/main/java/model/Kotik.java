package model;

public class Kotik {

    //variables
    private int prettiness; // привлекательность
    private int weight; // вес
    private String name; // Имя
    private String meow; // Звук "мяу"
    private int count; // количество созданных объектов
    private int satiety; // сытость

    private boolean isSatiety() {
        if (this.satiety <= 0) {
            return false;
        } else {
            return true;
        }
    }

    // create class object
    public Kotik() {

        count++;
    }

    public Kotik(int prettiness, int weight, String name, String meow, int satiety) {
        count++;
        this.prettiness = prettiness;
        this.weight = weight;
        this.name = name;
        this.meow = meow;
        this.satiety = satiety;
    }

    public void setKotik(int prettiness, int weight, String name, String meow, int satiety) {
        this.prettiness = prettiness;
        this.weight = weight;
        this.name = name;
        this.meow = meow;
        this.satiety = satiety;
    }

    // method 1 - принимает в параметры переменную игрушки, с которой будет играть
    public boolean play(String toy) {
        if (isSatiety()) {
            System.out.println(name + " playing with a " + toy);
            return true;
        } else {
            System.out.println(name + " wants to eat! Feed him!");
            return false;
        }
    }

    // method 2 - принимает в параметры переменную места, где будет спать
    public boolean sleep(String place) {
        if (isSatiety()) {
            System.out.println(name + " is sleeping on the " + place);
            return true;
        } else {
            System.out.println(name + " wants to eat! Feed him!");
            return false;
        }
    }

    // method 3 - не принимает параметров, описывает поведение котика - гонится за мышкой
    public boolean chaseMouse() {
        if (isSatiety()) {
            System.out.println(name + " chase mouse");
            return true;
        } else {
            System.out.println(name + " wants to eat! Feed him!");
            return false;
        }
    }

    // method 4 - перегрузка 1 (принимает количество сытости,увеличивает переменную)
    public boolean eat(int satiety) {
        this.satiety += satiety;
        System.out.println(name + " satiety: " + this.satiety);
        return true;
    }

    // method 4 - перегрузка 2 (принимает сытость и название еды)
    public boolean eat(int satiety, String food) {
        this.satiety += satiety;
        System.out.println(name + " ate the " + food + " and now satiety: " + this.satiety);
        return true;
    }

    // method 4 - перегрузка 3 (без параметров, вызовает перегрузку 2 с парматерами сытость и еда)
    public boolean eat() {
        return eat(2, "dry food");
    }

    // method 5 - не принимает параметров, описывает поведение котика - мяукает
    public boolean sayMeow() {
        if (isSatiety()) {
            System.out.println(name + " say " + meow);
            return true;
        } else {
            System.out.println(name + " wants to eat! Feed him!");
            return false;
        }
    }

    // method x - не принимает параметров, описывает поведение котика в течение дня
    public void liveAnotherDay() {
        for (int i = 1; i <= 24; i++) {
            double rand_number;
            rand_number = (Math.random() * 5 + 1);
            switch ((int) rand_number) {
                case (1): {
                    play("toy mouse");
                    break;
                }
                case (2): {
                    sleep("sofa");
                    break;
                }
                case (3): {
                    chaseMouse();
                    break;
                }
                case (4): {
                    eat();
                    break;
                }
                case (5): {
                    sayMeow();
                    break;
                }
            }
        }
    }

    // other methods
    public int getWeight() {

        return this.weight;
    }

    public String getName() {

        return this.name;
    }

    public int getCount() {

        return this.count;
    }

    public String getMeow() {

        return this.meow;
    }
}

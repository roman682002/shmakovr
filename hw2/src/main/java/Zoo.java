import animals.*;
import food.*;
import worker.*;

public class Zoo {

    public static void main(String[] args) {

        // Создание животных
        Camel camel = new Camel();
        Duck duck = new Duck();
        Zebra zebra = new Zebra();
        Fish fish = new Fish();
        Lion lion = new Lion();
        Harpy harpy = new Harpy();


        // Создание еды для животных
        Food chicken = new Meat();
        Food beef = new Meat();
        Food cabbage = new Grass();
        Food potatoes = new Grass();

        // Создание рабочего
        Worker Anton = new Worker();

        // Рабочий кормит животных
        Anton.feed(potatoes, lion);
        Anton.feed(beef, harpy);
        Anton.feed(potatoes, fish);
        Anton.feed(cabbage, camel);
        Anton.feed(chicken, duck);

        // Рабочий просит поговорить животных
        Anton.getVoice(lion);
//      Anton.getVoice(fish); - программа не компилируется
        Anton.getVoice(zebra);
        Anton.getVoice(duck);
        Anton.getVoice(harpy);

        // Пруд с животными, которые умеют плавать (массив)
        Swim[] swims = new Swim[5];
        swims[0] = lion;
        swims[1] = camel;
        swims[2] = zebra;
        swims[3] = fish;
        swims[4] = duck;

        // каждое животное плавает по очереди
        for (int i = 0; i < swims.length; i++) {
            swims[i].swim();
        }
    }
}






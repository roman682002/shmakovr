package food;

public class Meat extends Food {

    @Override
    public String nameFood() {
        return "мясо";
    }
    @Override
    public String nameAnotherFood() {
        return "овощи";
    }

    @Override
    public String nameAnother() {
        return "травоядному";
    }

    @Override
    public int addSatiety() {
        return 10;
    }

    @Override
    public String nameAnimal() {
        return "хищнику";
    }
}

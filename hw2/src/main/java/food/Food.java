package food;

public abstract class Food {

    public abstract String nameFood();
    public abstract String nameAnotherFood();
    public abstract String nameAnimal();
    public abstract String nameAnother();
    public abstract int addSatiety();
}





package food;

public class Grass extends Food {

    @Override
    public String nameFood() {
        return "овощи";
    }

    @Override
    public String nameAnotherFood() {
        return "мясо";
    }

    @Override
    public String nameAnother() {
        return "хищнику";
    }

    @Override
    public int addSatiety() {
        return 5;
    }

    @Override
    public String nameAnimal() {
        return "травоядному";
    }

}

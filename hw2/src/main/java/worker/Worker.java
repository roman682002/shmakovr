package worker;

import animals.Animal;
import animals.Voice;
import food.Food;


public class Worker {

    public void feed(Food food, Animal animal) {
        animal.eat(food);

    }

    public void getVoice(Voice animal) {
        System.out.println(animal.voice());
    }

}
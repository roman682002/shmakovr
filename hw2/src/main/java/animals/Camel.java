package animals;

// верблюд
public class Camel extends Herbivorous implements Voice, Swim, Run {

    @Override
    public String voice() {
        return "Верблюд говорит УАААА";
    }

    @Override
    public boolean swim() {
        System.out.println("Верблюд поплыл");
        return true;
    }

    @Override
    public boolean run() {
        System.out.println("Верблюд побежал");
        return true;
    }

}

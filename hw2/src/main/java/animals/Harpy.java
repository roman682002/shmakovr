package animals;

// гарпия
public class Harpy extends Carnivorous implements Voice, Run, Fly {

    @Override
    public String voice() {
        return "Гарпия говорит АИАИАИАИАИ";
    }

    @Override
    public boolean run() {
        System.out.println("Гарпия побежала");
        return true;
    }

    @Override
    public boolean fly() {
        System.out.println("Гарпия полетела");
        return true;
    }
}

package animals;
// хищник

import food.*;

public abstract class Carnivorous extends Animal {

    private int satiety = 0;

    @Override
    public void eat(Food food) {

        if (food instanceof Meat) {
            System.out.println("Удача, " + food.nameFood() + " подходит " + food.nameAnimal()
                    + ", он благодарно смотрит на вас, потому что теперь сыт.");
            addSatiety(food);
            System.out.println("Его сытость составляет: " + getSatiety());
            return;
        }

        System.out.println(food.nameAnother() + " не подходят " + food.nameFood() + "! Он недовольно" +
                " смотрит на вас, потому что вы опять принесли не то...");
        System.out.println("Прийдется принести "+ food.nameAnotherFood());
        System.out.println("Его сытость по прежнему составляет: " + getSatiety());
 }


    public void addSatiety(Food food) {
        satiety += food.addSatiety();
    }

    public int getSatiety() {
        return satiety;
    }
}



package animals;

// утка
public class Duck extends Herbivorous implements Voice, Swim, Run, Fly {

    @Override
    public String voice() {
        return "Утка говорит КРЯЯЯЯ";
    }

    @Override
    public boolean swim() {
        System.out.println("Утка поплыла");
        return true;
    }

    @Override
    public boolean run() {
        System.out.println("Утка побежала");
        return true;
    }

    @Override
    public boolean fly() {
        System.out.println("Утка полетела");
        return true;
    }
}

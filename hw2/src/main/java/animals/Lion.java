package animals;

// Лев
public class Lion extends Carnivorous implements Voice, Swim, Run {

    @Override
    public String voice() {
        return "Лев говорит РАААААААР";
    }

    @Override
    public boolean swim() {
        System.out.println("Лев поплыл");
        return true;
    }

    @Override
    public boolean run() {
        System.out.println("Лев побежал");
        return true;
    }
}



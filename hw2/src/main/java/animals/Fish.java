package animals;

// рыба
public class Fish extends Carnivorous implements Swim {

    @Override
    public boolean swim() {
        System.out.println("Рыба поплыла");
        return true;
    }
}

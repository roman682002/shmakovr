package animals;

// зебра
public class Zebra extends Herbivorous implements Voice, Swim, Run {

    @Override
    public String voice() {
        return "Зебра говорит УАУАУАУА";
    }

    @Override
    public boolean swim() {
        System.out.println("Зебра поплыла");
        return true;
    }

    @Override
    public boolean run() {
        System.out.println("Зебра побежала");
        return true;
    }
}
